# Docker Workshop
Lab 05: Building your first image

---

## Preparations

 - Create a new folder for the lab:
```
mkdir lab-05
cd lab-05
```

## Instructions

 - Create a new file called "**Dockerfile**" with the following content:
```
FROM selaworkshops/alpine_new:3.4
RUN apk add --no-cache python
CMD python -m SimpleHTTPServer 5000
```

 - Build the image using the command:
```
docker build -t python-app:1.0 .
```

 - Ensure the image was created:
```
docker images
```

 - Run the created image in interactive mode to attach to the container process:
```
docker run -it --name pythonlab3 python-app:1.0
```

 - Browse to the port 5000 to check if you can reach the application:
```
http://<your-server's-ip>:5000
```

 - It is not working, **That is fine**!
 - The port 5000 is not exposed to the host therefor we can not reach the application. We will see how to expose the container's ports in the **next module**.

 - Exit from the container using:
```
CTRL + C
```

 - Now, Docker run the same image but pass a command as "run parameters":
```
docker run -it python-app:1.0 "echo" "Proof of override command!"
```

 - As you may have noticed the command inside the container was overridden by the command passed as parameter in the docker run command
 
